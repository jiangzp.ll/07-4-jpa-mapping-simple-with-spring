package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ProductController {

    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @PostMapping("/api/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest productRequest) {
        Product product = new Product(productRequest.getName(), productRequest.getPrice(), productRequest.getUnit());
        @Valid Product saveProduct = productRepository.save(product);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location",
                String.format("http://localhost/api/products/%d", saveProduct.getId())).build();
    }

    @GetMapping("/api/products/{productId}")
    public ResponseEntity<GetProductResponse> getProduct(@PathVariable(value = "productId") Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if (!productOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.ok(new GetProductResponse(productOptional.get()));
        }
    }

}
